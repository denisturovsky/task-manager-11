package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project task);

    void clear();

}
