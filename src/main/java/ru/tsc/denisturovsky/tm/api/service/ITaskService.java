package ru.tsc.denisturovsky.tm.api.service;

import ru.tsc.denisturovsky.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);

    Task remove(Task task);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    void clear();

}
